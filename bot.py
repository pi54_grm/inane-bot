import discord
import asyncio
import random
import os
import pickle
import json
from discord.ext import commands
from discord.ext.commands import Bot
from lib import fighter
import operator
import datetime
import pymongo
from pymongo import MongoClient

client = MongoClient('--Connection String--')
db_players = client.players.players
col = list(db_players.find())
#players = col
db_homes = client.homes.homes
col = list(db_homes.find())
homes = col[0]

bot = commands.Bot(command_prefix='-')

@bot.event
async def on_ready():
    print("----------")
    print("Logged in as:")
    print("    "+str(bot.user.name))
    print("    "+str(bot.user.id))
    print("----------")
    update_rep()
    await bot.change_presence(game=discord.Game(name='кару грешников'))

@bot.command(pass_context=True)
async def faq(ctx):
    msg = "У вас есть прекрасная возможность стать часть этого мира! Если вас примет один из домов, то вы сможете сражаться с другими членам домов, чтобы повышать свою репутацию и репутацию дома."
    msg+= "Также у вас появится магия (особенная для каждого дома), чтобы побеждать своих врагов и защищать чеcть своих сопартийцев. "
    em = discord.Embed(title="Добро пожаловать в мир Инанэ!", description=msg)
    await bot.send_message(ctx.message.channel, embed=em)
    msg = "**В: Как вступить в дом?**\n*О: Нужно оставить заявку в #guild .*\n"
    msg+= "**В: Смогу ли я вступить в дом, если не буду участвовать в PvP?**\n*О: Это Вы можете узнать только у лидера дома, в который вы хотите вступить.*\n"
    msg += "**В: Можно ли получать репутацию другим способом?**\n*О: Да, можно использовать ежедневную команду.*"
    em = discord.Embed(title="F.A.Q.", description=msg)
    await bot.send_message(ctx.message.channel, embed=em)

@bot.command(pass_context=True)
async def команды(ctx):
    msg = "**-я** - своя информация\n"
    msg+= "**-дуэль @<имя_игрока>** - вызов игрока на дуэль\n"
    msg += "**-честь** - ежедневный бонус репутации\n"
    msg += "**-улучшить** - улучшения своих параметров за счет очков опыта\n"
    em = discord.Embed(title="Команды", description=msg)
    await bot.send_message(ctx.message.channel, embed=em)
@bot.command(pass_context=True)
async def спасти(ctx, user: discord.Member):
    if ctx.message.author.id == '152120975607988224':
        Title1 = 'Приветствую тебя, новобранец!'
        Description1 = 'От имени дома **Вурн**, разреши *тебя* пригласить в наши ряды!  Если *ты* готов помогать другим и защищать своих братьев и сестр - напиши в чат __**Защищать**__!'
        Name1 = 'От дома Вурн'
        KeyWord = 'Защищать'
        Title2 = 'Поздравление!'
        Description2 = 'От имени дома **Вурн**, поздравляю тебя с прибытием! Делай добро своему дому и не забывай про остальных.'
        Name2 = 'От дома Вурн'
        await invite(ctx, user, 'Vurn', Title1, Description1, Name1, KeyWord, Title2, Description2, Name2)

@bot.command(pass_context=True)
async def алый(ctx, user: discord.Member):
    if ctx.message.author.id == '265210296904187905':
        Title1 = 'Добрейшего вам дня, всегда рады видеть в нашем доме новых друзей!'
        Description1 = 'От лица дома **Ликорис**, хочу поприветствовать Вас в нашей обитель, я не сомневаюсь, что вы станете достойным ее представителем.  Если вы готовы словно красная лилия распуститься даря людям добро и поддержку напишите в чат __**Цвести**__!'
        Name1 = 'От дома Ликорис'
        KeyWord = 'Цвести'
        Title2 = 'Cо всей душой приветствую тебя!'
        Description2 = 'Добро пожаловать в наш уютный дом Ликорис!'
        Name2 = 'От дома Ликорис'
        await invite(ctx, user, 'Lycoris', Title1, Description1, Name1, KeyWord, Title2, Description2, Name2)


@bot.command(pass_context=True)
async def во(ctx, arg1, arg2, user: discord.Member):
    if ctx.message.author.id == '185807264458735617':
        Title1 = 'Приветствую тебя, ученик!'
        Description1 = 'С благословления светлых богов приглашаю тебя в ряды нашей светлой армии!  Если ты готов сражаться за светлую идея, которая оправдывает любые средства - напиши в чат __**Готов**__!'
        Name1 = 'От дома Санлайт'
        KeyWord = 'Готов'
        Title2 = 'Поздравление!'
        Description2 = 'От имени дома Санлайт, благословляю тебя на светлые дела! Делай добро своему дому и давай отпор тем кто будет пытаться опорочить его славное имя.'
        Name2 = 'От дома Санлайт'
        await invite(ctx, user, 'Sunlight', Title1, Description1, Name1, KeyWord, Title2, Description2, Name2)
        #leave#


@bot.command(pass_context=True)
async def изгнать(ctx, user: discord.Member):
    if ctx.message.author.id == '152120975607988224':
        await leave('Vurn', user, ctx)
    elif ctx.message.author.id == '265210296904187905':
        await leave('Lycoris', user, ctx)
    elif ctx.message.author.id == '185807264458735617':
        await leave('Sunlight', user, ctx)

@bot.command(pass_context=True)
async def дом(ctx, home: str):
    if not homes.get(home) is None:
        msg = ''
        em = discord.Embed(title='Дом '+home, description=msg,
                           colour=int(homes[home]['colour'], 16)+0x200)
        #for users in homes[home]["users"]:
            #em.add_field(name= ctx.message.server.get_member(users).name,value = "{}".format(homes[home]["dignitys"][players[ctx.message.server.get_member(users).id]['dignity']]))
            #em.add_field(name= "Репутация",value = "{}".format(players[ctx.message.server.get_member(users).id]['reputation']))

        await bot.send_message(ctx.message.channel, embed=em)


@bot.command(pass_context=True)
async def дома(ctx):
    for users in homes:
        if not users == '_id':
            await bot.say(users + " {}".format(homes[users]['reputation']))

@bot.command(pass_context=True)
async def пм(ctx):
    botmsg = await bot.send_message(ctx.message.author, "test")
    await bot.add_reaction(botmsg,'⚔')
    await bot.add_reaction(botmsg,'👍')
    msg = await bot.wait_for_reaction(message=botmsg, user=ctx.message.author)
    print(msg[0].emoji)
    await bot.send_message(ctx.message.author, "asdad")


@bot.command(pass_context=True)
async def дуэль(ctx, user : discord.Member):
    player1 = db_players.find_one({"_id": ctx.message.author.id})
    player2 = db_players.find_one({"_id": user.id})
    if ctx.message.channel.id == '428642753249804308':

        em  = discord.Embed(title = "{} vs {}".format(player1["weapons"]['weapon'][1], player2["weapons"]['weapon'][1]),description="{}, Вас вызывают на дуэль! Если вы согласны сразиться, то поставте реакцию 👍. Иначе поставте 👎".format(user.mention))
        power = player1["СЛ"] + player1["ЛВ"] + player1["ИН"] + player1["ЗД"] + player1["weapons"]['weapon'][2] + player1["weapons"]['defence'][2]
        em.add_field(name=homes[player1['home']]["dignitys"][player1['dignity']] + " " + ctx.message.author.name, value = "Сила: " + str(power))
        power = player2["СЛ"] + player2["ЛВ"] + player2["ИН"] + player2["ЗД"] + player2["weapons"]['weapon'][2] + player2["weapons"]['defence'][2]
        em.add_field(name=homes[player2['home']]["dignitys"][player2['dignity']] + " "+user.name, value = "Сила: " +  str(power))
        msg = await bot.send_message(ctx.message.channel, embed=em)
        await bot.add_reaction(msg,'👍')
        await bot.add_reaction(msg,'👎')
        wait = await bot.wait_for_reaction(message = msg, user=user, timeout = 120)
        if wait is None or wait[0].emoji == '👎':
            new_em  = discord.Embed(description="{}, не подтвердил свое участие в дуэли.".format(user.mention))
            await bot.send_message(ctx.message.channel, embed=new_em)
        elif wait[0].emoji == '👍':
            await battle(ctx.message.author, user)

@bot.command(pass_context=True)
async def я(ctx):
    profile = db_players.find_one({"_id": ctx.message.author.id})
    if ctx.message.channel.id == homes[profile["home"]]["channel"]:
        msg = '**Сила:** *{}*\n'.format(profile['СЛ'])
        msg += '**Ловкость:** *{}*\n'.format(profile['ЛВ'])
        msg += '**Интелект:** *{}*\n'.format(profile['ИН'])
        msg += '**Здоровье:** *{}*\n'.format(profile['ЗД'])
        msg += '**Оружие:** *{name}* **{power}**\n'.format(name = profile['weapons']['weapon'][1], power = '+' + str(profile['weapons']['weapon'][2]) if profile['weapons']['weapon'][2] > 0 else profile['weapons']['weapon'][2])
        if profile['weapons']['defence'][0] == 'shield':
            msg += '**Щит:** *{name}* **{power}**\n'.format(name = profile['weapons']['defence'][1], power = '+' + str(profile['weapons']['defence'][2]) if profile['weapons']['defence'][2] > 0 else profile['weapons']['defence'][2])
        msg += '**Репутация:** *{}*\n'.format(profile['reputation'])
        msg += '**Опыт:** *{}*\n'.format(profile['exp'])
        em = discord.Embed(description=msg, colour=int(homes[profile['home']]['colour'], 16)+0x200)
        em.set_author(name=homes[profile['home']]["dignitys"][profile['dignity']] + " "+ctx.message.author.name,
                        icon_url=ctx.message.author.avatar_url)
        await bot.send_message(ctx.message.channel, embed=em)

@bot.command(pass_context=True)
async def улучшить(ctx):
    profile = db_players.find_one({"_id": ctx.message.author.id})
    if ctx.message.channel.id == homes[profile["home"]]["channel"]:
        em  = discord.Embed(title = 'Улучшение навыков')
        msg = '**Сила:** {}\n'.format(profile['СЛ'])
        msg += '**Ловкость:** {}\n'.format(profile['ЛВ'])
        msg += '**Интелект:** {}\n'.format(profile['ИН'])
        msg += '**Здоровье:** {}\n'.format(profile['ЗД'])
        msg += '**Очки опыта:** {}\n'.format(profile['exp'])
        em.add_field(name= "Ваши параметры",value = msg)
        em.add_field(name= "Стоимость параметров",value = "🇸 **Сила:** 2000 exp\n🇩 **Ловкость:** 2000 exp\n🇮 **Интелект:** 1000 exp\n🇭 **Здоровье:** 1000 exp")
        msg1 = await bot.send_message(ctx.message.channel, embed=em)
        await bot.add_reaction(msg1,'🇸')
        await bot.add_reaction(msg1,'🇩')
        await bot.add_reaction(msg1,'🇮')
        await bot.add_reaction(msg1,'🇭')
        reaction = await bot.wait_for_reaction(message = msg1, user=ctx.message.author, timeout = 120)
        if reaction[0].emoji == '🇸':
            if profile['exp'] < 2000:
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вам не хватает опыта для повышения характерисики!")
            else:
                profile["СЛ"] +=1
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вы повысили **силу**")
        elif reaction[0].emoji == '🇩':
            if profile['exp'] < 2000:
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вам не хватает опыта для повышения характерисики!")
            else:
                profile["ЛВ"] +=1
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вы повысили **ловкость**")
        elif reaction[0].emoji == '🇮':
            if profile['exp'] < 1000:
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вам не хватает опыта для повышения характерисики!")
            else:
                profile["ИН"] +=1
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вы повысили **интелект**")
        elif reaction[0].emoji == '🇭':
            if profile['exp'] < 1000:
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вам не хватает опыта для повышения характерисики!")
            else:
                profile["ЗД"] +=1
                em_atr  = discord.Embed(title = 'Улучшение навыков', description = "Вы повысили **здоровье**")
        await bot.send_message(ctx.message.channel, embed=em_atr)
        db_players.save(profile)


@bot.command(pass_context=True)
async def честь(ctx):
    player = db_players.find_one({"_id": ctx.message.author.id})
    if ctx.message.channel.id == homes[player["home"]]["channel"]:
        date_user = datetime.datetime.strptime(player["rep_last_date"],"%Y%m%d")
        now = datetime.datetime.now()
        if (date_user + datetime.timedelta(days = 1)) <= now:
            rep = round((player["reputation"] * 0.1) + 10,2)
            player["reputation"] += rep
            player["rep_last_date"] =now.strftime("%Y%m%d")
            await bot.say("*{} получил {} очков репутации*".format(ctx.message.author.mention,rep))
            db_players.save(player)
            update_rep()
            update_dignity(ctx.message.author)
        else:
            await bot.say("Вы уже использовали сегодня 'честь'")

async def leave(home, user, ctx):
    if user.id in homes[home]['users']:
            homes[home]['users'].remove(user.id)
            db_homes.save(homes)
            db_players.remove({"_id": user.id})
            role = discord.utils.get(ctx.message.server.roles, name=home)
            await bot.remove_roles(user, role)
            await bot.say('{} Вас изгнали из дома!'.format(user.mention))
    else:
        await bot.say('Милсдарь, нету такого холопа в Вашем доме')

async def invite(ctx, user, home, Title1, Description1, Name1, KeyWord, Title2, Description2, Name2):
    if db_players.find_one({"_id": user.id}) is None:
        em = discord.Embed(title=Title1,
                            description=Description1, colour=int(homes[home]['colour'], 16)+0x200)
        em.set_author(name=Name1,
                        icon_url=ctx.message.author.avatar_url)
        eq = sum_user(home)
        if eq == True:
            em.set_footer(text= 'Внимание! В этом доме намного больше участников, чем в других. Это означает, что вы будете получать намного меньше репутации!')
        await bot.send_message(bot.get_channel('267730713833504769'), user.mention, embed=em)
        wait_masg = await bot.wait_for_message(author=user, content=KeyWord, timeout = 120)
        if not wait_masg is None:
            role = discord.utils.get(ctx.message.server.roles, name=home)
            await bot.add_roles(user, role)
            homes[home]['users'].append(user.id)
            db_homes.save(homes)
            new_players = homes[home]['start']
            new_players["_id"] = user.id
            new_players['rep_last_date'] = (datetime.datetime.now() - datetime.timedelta(days = 1)).strftime("%Y%m%d")
            em = discord.Embed(
                title=Title2, description=Description2, colour=int(homes[home]['colour'], 16)+0x200)
            em.set_author(name=Name2,
                        icon_url=ctx.message.author.avatar_url)
            db_players.insert(new_players)
            await bot.send_message(bot.get_channel('267730713833504769'), user.mention, embed=em)
            await choose_weapon(home, user)



async def battle_pm_msg(fighter, user):
    if fighter.waiting_for_turn is True:
        msg = await get_action(fighter)
        wait_reaction = await bot.send_message(user, 'Ваше здоровья: {}\n'.format(fighter.hp) + msg['all'] )
        for key in msg:
            if not key == 'all':
                await bot.add_reaction(wait_reaction,key)
        action = await bot.wait_for_reaction(message= wait_reaction, user=user)
        return action[0].emoji
    else:
        return '⏳'


async def battle(user1:discord.Member, user2:discord.Member):
    battle_channel = bot.get_channel('428642753249804308')
    fighter1 = fighter(user1)
    fighter2 = fighter(user2)
    player1 = db_players.find_one({"_id":user1.id})
    player2 = db_players.find_one({"_id":user2.id})
    msg_player = None
    while fighter1.hp > 0 and fighter2.hp > 0:
        process_effects(fighter1)
        process_effects(fighter2)
        msg = discord.Embed(title = "{} vs {}".format(fighter1.user['home'],fighter2.user['home']), description  = "**{}**:❌ | **{}**:❌".format(user1.name, user2.name))
        if msg_player is None:
            msg_player = await bot.send_message(battle_channel, embed=msg)
        else:
            await bot.edit_message(msg_player, embed = msg)
        action1 = await battle_pm_msg(fighter1, user1)
        msg = discord.Embed(title = "{} vs {}".format(fighter1.user['home'],fighter2.user['home']), description  = "**{}**:✅ | **{}**:❌".format(user1.name, user2.name))
        await bot.edit_message(msg_player, embed = msg)
        action2 = await battle_pm_msg(fighter2, user2)
        msg = discord.Embed(title = "{} vs {}".format(fighter1.user['home'],fighter2.user['home']), description  = "**{}**:✅ | **{}**:✅ ".format(user1.name, user2.name))
        await bot.edit_message(msg_player, embed = msg)
        actions1 = await fighter1.get_turn_result(fighter2, action1)
        asyncio.sleep(1)
        actions2 = await fighter2.get_turn_result(fighter1, action2)
        reset_cooldown(fighter1)
        reset_cooldown(fighter2)
        await fight(actions1, actions2, fighter1, fighter2)
    if fighter1.hp > fighter2.hp:
        msg = discord.Embed(title = "**{}** vs ~~{}~~".format(fighter1.user['home'],fighter2.user['home']), colour = int(homes[fighter1.user['home']]['colour'], 16)+0x200)
        if fighter1.user['home'] == fighter2.user['home']:
            exp  = round((random.randint(int(fighter1.rep),int(fighter2.rep) + int(fighter1.rep) ))*0.1,2)
            msg.add_field(name = user1.name, value = "Получает:\n💰{} очков опыта".format(exp))
            msg.add_field(name = user2.name, value = "Теряет:\n📈Ничего")
        else:
            exp  = random.randint(int(fighter1.rep),int(fighter2.rep) + int(fighter1.rep) )
            rep = round(10 * fighter2.rep/fighter1.rep,2)
            player1['reputation'] += rep
            player2['reputation'] -= rep
            msg.add_field(name = user1.name, value = "Получает:\n📈{} очков репутации\n💰{} очков опыта".format(rep, exp))
            msg.add_field(name = user2.name, value = "Теряет:\n📈{} очков репутации".format(rep))
        player1['exp'] += exp
    else:
        rep = round(10 * fighter1.rep/fighter2.rep,2)
        exp =  random.randint(int(fighter2.rep),int(fighter2.rep) + int(fighter1.rep) )
        msg = discord.Embed(title = "~~{}~~ vs **{}**".format(fighter1.user['home'],fighter2.user['home']), colour = int(homes[fighter2.user['home']]['colour'], 16)+0x200)
        if fighter1.user['home'] == fighter2.user['home']:
            exp  = (random.randint(int(fighter2.rep),int(fighter2.rep) + int(fighter1.rep) ))*0.1
            msg.add_field(name = user2.name, value = "Получает:\n📈{} очков опыта".format(exp))
            msg.add_field(name = user1.name, value = "Теряет:\n📈Ничего")
        else:
            exp  = random.randint(int(fighter2.rep),int(fighter1.rep) + int(fighter2.rep) )
            rep = round(10 * fighter1.rep/fighter2.rep,2)
            player2['reputation'] += rep
            player1['reputation'] -= rep
            msg.add_field(name = user2.name, value = "Получает:\n📈{} очков репутации\n💰{} очков опыта".format(rep, exp))
            msg.add_field(name = user1.name, value = "Теряет:\n📈{}очков репутации".format(rep))
        player2['exp'] += exp
    db_players.save(player1)
    db_players.save(player2)


    await bot.send_message(battle_channel, embed=msg)
    update_rep()
    update_dignity(user1)
    update_dignity(user2)

async def get_action(fighter):
    actions = {}
    if fighter.weapons['weapon'][3] is True:
        actions['⚔'] = ["⚔ Атаковать ({}%)".format(round(((fighter.user['ЛВ']+fighter.weapons['weapon'][2])/18)*100,2))]
    if fighter.weapons['defence'][3] is True:
        if fighter.weapons['defence'][0] == 'shield':
            actions['🛡'] = ["🛡 Защищаться ({}%)".format(round(((fighter.user['ЛВ'] +fighter.weapons['defence'][2])/18)*100,2))]
        elif (fighter.weapons['defence'][0] == 'duo') or (fighter.weapons['defence'][0] ==  'solo'):
            actions['🛡'] = ["🛡 Парировать ({}%)".format(round(((fighter.user['ЛВ'] +fighter.weapons['defence'][2])/18)*100,2))]
    if fighter.weapons['skill'][2] is True:
        actions['🔮'] = ["🔮 "+fighter.weapons['skill'][0]+" ({}%)".format(round(((fighter.user['ИН'] +fighter.weapons['skill'][2])/18)*100,2))]
    actions['⏳'] = ["⏳ Ожидать"]

    actions['all'] = ''
    for key in actions:
        if not key == 'all':
            actions['all']+= actions[key][0] + '\n'
    return actions

async def fight(action1, action2, fighter1, fighter2):
    battle_channel = bot.get_channel('428642753249804308')
    actions  = [action1, action2]
    for action in sorted(actions, key=operator.attrgetter(('priority'))):
        action.before_execute(fighter1, fighter2)
        if action.successful:
            action.execute(fighter1, fighter2)
            msg = discord.Embed(title = action.fighter.user['home'], description  = action.successful_msg, colour=int(homes[action.fighter.user['home']]['colour'], 16)+0x200)
        else:
            msg = discord.Embed(title = action.fighter.user['home'], description  = action.failed_msg, colour=int(homes[action.fighter.user['home']]['colour'], 16)+0x200)
        if action.fighter.msg_fighter is None:
               action.fighter.msg_fighter = await bot.send_message(battle_channel, embed=msg)
        else:
            await bot.edit_message(action.fighter.msg_fighter, embed=msg)



async def update(user_id):
    profile = db_players.find_one({'_id':user_id})
    profile['speed'] = profile['ЛВ'] + profile['ЗД']
    profile['power'] =  profile['ЛВ'] + profile['ЗД'] + profile['СЛ'] + profile['ИН'] + profile['weapons']['weapon'][2]-40
    if not profile['weapons']['weapon'][1] == profile['weapons']['defence'][1]:
        profile['power'] += profile['weapons']['defence'][2]
    db_players.save(profile)

def reset_cooldown(fighter):
    if fighter.weapons['weapon'][0] == 'duo':
        if fighter.weapons['weapon'][3] == False:
             fighter.weapons['weapon'][3] = True
    if fighter.weapons['defence'][0] == 'duo':
        if fighter.weapons['defence'][3] == False:
            fighter.weapons['defence'][3] = True

def process_effects(fighter):
    for x in fighter.buff:
        x.duration -=1
        if x.duration <= 0:
            fighter.buff.remove(x)
    for x in fighter.debuff:
        x.execute()
        if x.duration <= 0:
            x.execute()
            fighter.debuff.remove(x)
        x.duration -=1

def update_rep():
    for home in homes:
        if not home == '_id':
            homes[home]['reputation'] = 0.0
            if len(homes[home]["users"]) > 0:
                for player in homes[home]["users"]:
                    tmp_player = db_players.find_one({"_id":player})
                    homes[home]['reputation']+= round(tmp_player["reputation"] / len(homes[home]['users']), 2)
    db_homes.save(homes)
def update_dignity(user):
    player = db_players.find_one({"_id":user.id})
    if player["reputation"] <100:
        player["dignity"] = 0
        player["weapons"]['weapon'][2] = 0
        player["weapons"]['defence'][2] = 0
    elif player["reputation"] <200:
        player["dignity"] = 1
        player["weapons"]['weapon'][2] = 1
        player["weapons"]['defence'][2] = 1
    elif player["reputation"] < 300:
        player["dignity"] = 2
        player["weapons"]['weapon'][2] = 2
        player["weapons"]['defence'][2] = 2
    elif player["reputation"] <400:
        player["dignity"] = 3
        player["weapons"]['weapon'][2] = 3
        player["weapons"]['defence'][2]= 3
    if homes[player["home"]]['owner'] == user.id:
        player["dignity"] = 4
    db_players.save(player)
def update_all():
    db_homes.save(homes)

async def choose_weapon(home, user):
    player = db_players.find_one({"_id": user.id})
    if home == 'Vurn':
        em = discord.Embed(title='Дом '+home, description="{}, ты должен выбрать тип оружия, которое останится с тобой **навсегда**. Да будет твой выбор верным!".format(user.mention),
                           colour=int(homes[home]['colour'], 16)+0x200)
        em.add_field(name='🛡️ Одноручный меч и щит',
                     value="У вас есть возможность атаковать или защищаться каждый ход")
        em.add_field(name="⚔ Двуручный меч", value="У вас есть возможность атаковать или защищаться, но после каждого такого действия вам требуется подождать один ход.\nВзаамен, вы получаете больший бонус к атаке.")
        msg_weapons = await bot.send_message(bot.get_channel(homes[home]['channel']), embed=em)
        await bot.add_reaction(msg_weapons, '🛡')
        await bot.add_reaction(msg_weapons, '⚔')
        action = await bot.wait_for_reaction(message= msg_weapons, user=user)
        if action[0].emoji == '🛡':
            player['weapons']['weapon'] = ["solo","Меч ",0,True,0]
            player['weapons']['defence'] = ["shield","Щит ",0,True,0]
            solo_em = discord.Embed(title='Дом '+home, description="Вы выбрали себе двух помошников - **меч и щит**! Но ничто в этом мире не безымянно, поэтому ❗__**введите для начала имя меча**__❗",
                           colour=int(homes[home]['colour'], 16)+0x200)
            await bot.send_message(bot.get_channel(homes[home]['channel']), embed=solo_em)
            weapon_name = await bot.wait_for_message(author = user, channel = bot.get_channel(homes[home]['channel']))
            player['weapons']['weapon'][1] += weapon_name.content
            solo_em = discord.Embed(title='Дом '+home, description="Отлично! Теперь очеред **щита**. ❗__**Введите имя для щита**__❗",
                           colour=int(homes[home]['colour'], 16)+0x200)
            await bot.send_message(bot.get_channel(homes[home]['channel']), embed=solo_em)
            weapon_name = await bot.wait_for_message(author = user, channel = bot.get_channel(homes[home]['channel']))
            player['weapons']['defence'][1] += weapon_name.content
            last_em = discord.Embed(title = 'Дом '+home, description = 'Ты держишь в своих руках **{} и {}** ! Надеюсь ты сможешь защитить себя, своих близких, свой дом от чужих злодеяних и будешь осуществлять свои поступки только ради справедлиовсти во всем!'.format(player['weapons']['weapon'][1],player['weapons']['defence'][1]))
            await bot.send_message(bot.get_channel(homes[home]['channel']), embed=last_em)
        if action[0].emoji == '⚔':
            player['weapons']['weapon'] = ["duo","Двуручный Меч ",0,True,0]
            player['weapons']['defence'] = ["duo","Двуручный Меч ",0,True,0]
            solo_em = discord.Embed(title='Дом '+home, description="Вы выбрали себе массивного друга - **двуручный меч**! Но ничто в этом мире не безымянно, поэтому ❗__**введите имя для меча**__❗",
                           colour=int(homes[home]['colour'], 16)+0x200)
            await bot.send_message(bot.get_channel(homes[home]['channel']), embed=solo_em)
            weapon_name = await bot.wait_for_message(author = user, channel = bot.get_channel(homes[home]['channel']))
            player['weapons']['weapon'][1] += weapon_name.content
            player['weapons']['defence'][1] += weapon_name.content
            last_em = discord.Embed(title = 'Дом '+home, description = 'Ты держишь своего верного напариника **{}**! Надеюсь ты сможешь защитить себя, своих близких, свой дом от чужих злодеяних и будешь осуществлять свои поступки только ради справедлиовсти во всем!'.format(player['weapons']['weapon'][1]))
            await bot.send_message(bot.get_channel(homes[home]['channel']), embed=last_em)

    elif home == "Lycoris":
        em = discord.Embed(title='Дом '+home, description="{}, С приходом в наш дом  ты получаешь себе невероятной силы и такой же утонченный и красивый как и ты".format(user.mention),
                           colour=int(homes[home]['colour'], 16)+0x200)
        em.add_field(name='🏮 Боевой веер',
                     value="Твои атаки сильны, быстры и  изысканны,  ты можешь  очаровывать врага лишь взмахом веера  и оставаться произведением искусства даже в бою.")
        em.add_field(name="Бонус", value="Дает возможность  атаковать каждый ход с увеличенным уроном, но не имея возможности защититься\n\nТеперь ты держишь в руках свое оружие, которое останется с тобой навсегда, так назови же его! ❗__**Напиши в этот чат, какое имя ты хочешь дать своему вееру!**__❗")
        msg_weapons = await bot.send_message(bot.get_channel(homes[home]['channel']), embed=em)
        weapon_name = await bot.wait_for_message(author = user, channel = bot.get_channel(homes[home]['channel']))
        player['weapons']['weapon'] = ["fan","Веер "+weapon_name.content,0,True,0]
        last_em = discord.Embed(title = 'Дом '+home, description = 'Возьми же в  руки {},  никогда не расставайся с ним, верши свое слово над головами твоих врагов, очаровывай им людей, используй его с умом, его красота так же опасна как и его великолепие.'.format(player['weapons']['weapon'][1]))
        await bot.send_message(bot.get_channel(homes[home]['channel']), embed=last_em)
    elif home == "Sunlight":
        em = discord.Embed(title='Дом '+home, description="{}, добро пожаловать! Возьми это оружие, оно послужит тебе хорошую службу в битвах против наших врагов!".format(user.mention),
                           colour=int(homes[home]['colour'], 16)+0x200)
        em.add_field(name='☀ Боевой молот',
                     value="Сильное, крепкое оружие, специально созданое что бы уничтожать противников без тени сомнения. Возьми его, и почувствуй силу бога солнца!")
        em.add_field(name="Бонус", value="Твой бонус к меткости увеличен, но бонус от щита к защите аннулирован\n\nКаждый воин светлых богов может назвать свое оружие, и получить благословение!❗__**Как же ты назовешь свой молот? Напиши в чат**__❗")
        msg_weapons = await bot.send_message(bot.get_channel(homes[home]['channel']), embed=em)
        weapon_name = await bot.wait_for_message(author = user, channel = bot.get_channel(homes[home]['channel']))
        player['weapons']['weapon'] = ["hammer","Молот "+weapon_name.content,0,True,0]
        last_em = discord.Embed(title = 'Дом '+home, description = 'Отлично, новобранец! Возьми же свой молот, {}, и защищай с его помощью истину и свет!'.format(player['weapons']['weapon'][1]))
        await bot.send_message(bot.get_channel(homes[home]['channel']), embed=last_em)
    db_players.save(player)

def sum_user(home):
    homes_tmp = homes
    res = 0
    for x in homes_tmp:
        if not x is home:
            if not x == '_id':
                res +=len(homes[x]['users'])
    if res > len(homes[home]['users']):
        return False
    else:
        return True
