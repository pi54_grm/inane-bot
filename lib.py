import discord
import asyncio
import random
import os
import pickle
import json
from discord.ext import commands
from discord.ext.commands import Bot
import pymongo
from pymongo import MongoClient

client = MongoClient('--Connection String--')
db_players = client.players.players



class fighter:
    def __init__(self, user: discord.Member):
        self.discord_member = user
        self.user = db_players.find_one({"_id":user.id})
        self.hp = self.user["ЗД"]
        self.weapons = self.user["weapons"].copy()
        self.waiting_for_turn = True
        self.debuff = []
        self.buff = []
        self.msg_fighter = None
        self.rep = self.user["ИН"]  + self.user["ЗД"] + self.user["ЛВ"] + self.user["СЛ"] + self.weapons["weapon"][2] + self.weapons["defence"][2]
    async def get_turn_result(self, fighter, choosen_action):
        if self.waiting_for_turn is True:
            actionsList = await self.get_action(self.weapons)
            return actionsList[choosen_action]
        else:
            return action_wait(self)

    async def get_action(self, weapons):
        actions = {}
        if self.weapons['weapon'][3] is True:
            actions['⚔'] = action_attack(self)
        if self.weapons['defence'][3] is True:
            actions['🛡'] = action_defence(self)
        if self.weapons['skill'][2] is True:
            if self.user['home'] == 'Vurn':
                actions['🔮'] = action_skill_vurn(self)
            if self.user['home'] == 'Sunlight':
                actions['🔮'] = action_skill_sunlight(self)
            if self.user['home'] == 'Lycoris':
                actions['🔮'] = action_skill_lycoris(self)
        actions['⏳'] = action_wait(self)
        return actions


class action_attack:
    def __init__(self, fighter):
        dice = random.randint(3, 18)
        target_result = (fighter.user['ЛВ'] + fighter.weapons['weapon'][4]+fighter.weapons['weapon'][2])
        if fighter.weapons['weapon'][0] == 'hammer':
            target_result = (fighter.user['ЛВ'] + fighter.weapons['weapon'][4]+fighter.weapons['weapon'][2] + fighter.weapons['weapon'][2])
        self.fighter = fighter
        self.successful = dice <= target_result
        self.priority = 9001
        self.successful_msg = ''
        self.failed_msg = '**{}** не смог попасть по своему врагу...'.format(fighter.discord_member.name)
    def execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1
        damage = int((random.randint(3, 18))/3) + fighter1.weapons['weapon'][2]
        if fighter1.weapons['weapon'] == 'duo' or fighter1.weapons['weapon'] == 'fan':
            damage*=1.5
            damage+=fighter1.weapons['weapon'][2]
        if not any(isinstance(x, buff_block) for x in fighter2.buff):
            fighter2.hp -= damage
            self.successful_msg = '**{}** нанес **{}** урона **{}**'.format(
                fighter1.discord_member.name, damage, fighter2.discord_member.name)
        else:
            self.successful_msg = '**{}** хотел нанести урон, но игрок **{}** заблокировал удар'.format(
            fighter1.discord_member.name, fighter2.discord_member.name)
    def before_execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1
        if fighter1.weapons['weapon'][0] == 'duo':
            fighter1.weapons['weapon'][3] = False
        if fighter1.weapons['defence'][0] == 'duo':
            fighter1.weapons['defence'][3] = False


class action_defence:
    def __init__(self, fighter):
        dice = random.randint(3, 18)
        target_result = (fighter.user['ЛВ'] + fighter.weapons['defence'][4]+fighter.weapons['weapon'][2])
        if fighter.weapons['weapon'][0] == 'hammer':
            target_result = (fighter.user['ЛВ'] + fighter.weapons['weapon'][4])
        self.successful = dice <= target_result
        self.priority = 100
        self.successful_msg = ''
        if fighter.weapons['defence'][0] == 'shield':
            self.failed_msg = '**{}** не смог защититься...'.format(fighter.discord_member.name)
        elif fighter.weapons['defence'][0] == 'duo':
            self.failed_msg = '**{}** не смог парировать...'.format(fighter.discord_member.name)
        self.fighter = fighter
    def execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1
        fighter1.buff.append(buff_block(fighter1))
        if fighter1.weapons['defence'][0] == 'shield':
            self.successful_msg = '**{}** успешно заблокировал атаку **{}**'.format(fighter1.discord_member.name, fighter2.discord_member.name)
        elif fighter1.weapons['defence'][0] == 'duo':
            self.failed_msg = '**{}** успешно парировал атаку.'.format(fighter1.discord_member.name)
    def before_execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1


class action_wait:
    def __init__(self, fighter):
        self.successful = True
        self.priority = 0
        if fighter.waiting_for_turn is True:
            self.successful_msg = '**{}** ожидает и его умения увеличивают свою силу'.format(fighter.discord_member.name)
        else:
            self.successful_msg = '**{}** слишком очарован, чтобы что-то сделать'.format(fighter.discord_member.name)
        self.fighter = fighter
    def execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1
        if fighter1.waiting_for_turn is True:
            fighter1.weapons['weapon'][4] += 1
            fighter1.weapons['defence'][4] += 1

    def before_execute(self, fighter1, fighter2):
        return None


class action_skill_vurn:
    def __init__(self, fighter):
        self.successful = True
        self.priority = 50
        self.successful_msg = ''
        self.fighter = fighter
    def execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1
        fighter1.weapons['skill'][2] = False
        heal = random.randint(1, fighter1.user['ИН'])
        fighter1.hp += heal
        self.successful_msg = 'Игрок **{}** почувствовал Единство со своим домом и востановил себе {} очков здоровья.'.format(fighter1.discord_member.name, heal)
    def before_execute(self, fighter1, fighter2):
        return None


class action_skill_lycoris:
    def __init__(self, fighter):
        dice = random.randint(3, 18)
        target_result = 20
        self.successful = dice <= target_result
        self.priority = 50
        self.successful_msg = ''
        self.failed_msg = '**{}** не смог очаровать своего врага...'.format(fighter.discord_member.name)
        self.fighter = fighter
    def execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1
        fighter2.debuff.append(debuff_skip_turn(fighter2))
        fighter1.weapons['skill'][2] = False
        self.successful_msg = 'Игрок **{}** очаровал своего врага и тот пропустит след. ход'.format(fighter1.discord_member.name)
    def before_execute(self, fighter1, fighter2):
        return None


class action_skill_sunlight:
    def __init__(self, fighter):
        dice = random.randint(3, 18)
        target_result = (fighter.user['ИН'] + fighter.weapons['skill'][1])
        self.successful = dice <= target_result
        self.priority = 50
        self.successful_msg = ''
        self.failed_msg = 'Боги не смогли защитить игрока **{}**...'.format(fighter.discord_member.name)
        self.fighter = fighter
    def execute(self, fighter1, fighter2):
        if self.fighter != fighter1:
            fighter1, fighter2= fighter2, fighter1
        fighter1.buff.append(buff_block(fighter1))
        damage = random.randint(1, 3)
        fighter2.hp -= damage
        fighter1.weapons['skill'][2] = False
        self.successful_msg = 'Боги даровали защиту игроку **{}** и наказали его врага **{}** нанеся **{}** урона'.format(
            fighter1.discord_member.name,  fighter2.discord_member.name,damage)
    def before_execute(self, fighter1, fighter2):
        return None


class debuff_skip_turn:
    def __init__(self, fighter):
        self.fighter = fighter
        self.duration = 1

    def execute(self):
        self.fighter.waiting_for_turn = False
        if self.duration <1:
            self.waiting_for_turn = True


class buff_block:
    def __init__(self, fighter):
        self.fighter = fighter
        self.duration = 1
    def execute(self):
        return None
